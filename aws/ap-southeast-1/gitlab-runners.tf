variable gitlab_runner_terraform_token {
  type        = string
  description = "Gitlab runner registration token for the terraform gitlab runner"
}

resource "aws_key_pair" "gitlab-runner" {
  key_name   = "gitlab-runner"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAr1TBBNbYF0vFZPUQn3Lqmwa7nLeUFC0IMkoCiUx13lP9hNSBtDWxzh9xTbPD/4aNHDIIkih0W30MteIo2asEDxixKtkEZR7LHKHLr3j2KvfD8H/mXmSI4OZGL9Gu5hFay+yBW5M1YoLelg63b15qJrdtHUIL2qpmq297P+z8IQLon2e65ii3RbrMaD4W1M1bF/4rFND0D9ZFk85MHQnb1MDAKIUHNSF+1k4tiGD13QkbQQW7rh5cfKyGPY3LwkMsoD0B4zLE/nqvF8M9pVCyVmrB9O8pCsHcTGefU0Fdvytco34X0Ue835gargj7nx3GmC4tbT04g3dy4lW8V0/LzQ=="
}

resource "aws_network_acl_rule" "internet-ingress-gitlab-runner" {
  network_acl_id = module.vpc-e2fyi.public_subnet_acl_ids

  rule_number = 155
  egress      = false
  protocol    = "tcp"
  rule_action = "allow"
  cidr_block  = "0.0.0.0/0"
  from_port   = 8093
  to_port     = 8093
}

resource "aws_network_acl_rule" "gitlab-runner-egress-internet" {
  network_acl_id = module.vpc-e2fyi.public_subnet_acl_ids

  rule_number = 155
  egress      = true
  protocol    = "tcp"
  rule_action = "allow"
  cidr_block  = "0.0.0.0/0"
  from_port   = 8093
  to_port     = 8093
}

module "gitlab-runner-terraform" {
  source  = "npalm/gitlab-runner/aws"
  version = "4.16.0"

  aws_region  = local.aws_region
  environment = "spot-runners"

  vpc_id                   = module.vpc-e2fyi.vpc_id
  subnet_id_runners        = values(module.vpc-e2fyi.azs_to_subnet_ids)[0]
  subnet_ids_gitlab_runner = values(module.vpc-e2fyi.azs_to_subnet_ids)
  ssh_key_pair             = aws_key_pair.gitlab-runner.key_name

  gitlab_runner_version         = "12.8.0"
  gitlab_runner_ssh_cidr_blocks = [module.vpc-e2fyi.cidr_block]

  runners_gitlab_url                = "https://gitlab.com/"
  runners_name                      = "terraform"
  runners_iam_instance_profile_name = aws_iam_instance_profile.terraform-apply.name
  runners_privileged                = true
  runners_use_private_address       = false
  runners_additional_volumes        = ["/var/run/docker.sock:/var/run/docker.sock"]
  runners_concurrent                = 1
  runners_executor                  = "docker+machine"

  gitlab_runner_registration_config = {
    registration_token = var.gitlab_runner_terraform_token
    tag_list           = "terraform,docker"
    description        = "gitlab runner for terraform with docker support"
    locked_to_project  = "false"
    run_untagged       = "false"
    maximum_timeout    = "3600"
  }

  instance_type                = "t3a.micro"
  docker_machine_instance_type = "t3a.small"

  cache_bucket_prefix   = "gitlab-runner-"
  cache_shared          = true
  cache_expiration_days = 1

  enable_cloudwatch_logging        = false
  enable_manage_gitlab_token       = true
  enable_gitlab_runner_ssh_access  = true
  enable_runner_ssm_access         = true
  enable_docker_machine_ssm_access = true
  enable_ping                      = true
  enable_asg_recreation            = false
  enable_eip                       = true

  tags = local.tags
}

resource "aws_iam_instance_profile" "terraform-apply" {
  name = "terraform-apply"
  role = aws_iam_role.terraform-apply.name
}

resource "aws_iam_role" "terraform-apply" {
  name = "terraform-apply"
  path = "/gitlab-runners/"

  assume_role_policy = data.aws_iam_policy_document.terraform-apply.json
}

resource "aws_iam_role_policy_attachment" "terraform-apply-sysadmin" {
  role       = aws_iam_role.terraform-apply.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

resource "aws_iam_role_policy_attachment" "terraform-apply-ssm" {
  role       = aws_iam_role.terraform-apply.name
  policy_arn = aws_iam_policy.gitlab-runner-terraform-apply.arn
}

data aws_iam_policy_document "terraform-apply" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      identifiers = ["ec2.amazonaws.com"]
      type        = "Service"
    }
  }

}

resource "aws_iam_policy" "gitlab-runner-terraform-apply" {
  name        = "gitlab-runner-terraform-apply"
  path        = "/"
  description = "iam policy for gitlab runner"
  policy      = data.aws_iam_policy_document.terraform-ssm.json
}

data aws_iam_policy_document "terraform-ssm" {

  statement {
    effect    = "Allow"
    actions   = ["ssm:*"]
    resources = ["arn:aws:ssm:ap-southeast-1:994161731130:*"]
  }

  statement {
    effect    = "Allow"
    actions   = ["iam:*", "glue:*", "athena:*"]
    resources = ["*"]
  }

}
